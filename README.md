4testers

Programowanie w Python - zadanie zaliczeniowe

Instrukcja
W module src znajdziesz plik shop.py Zawierający dwie klasy:


Product, który opisuje produkt w sklepie, z nazwą, ceną i zamawianą ilością

Order, który opsuje zamówienie w sklepie, z możliwością dodania produktu, policzenia całkowitej ceny, całkowitej ilości produktów oraz zakupu
Oprócz tego w module tests znajdują się testy do tych klas.
Niestety, zły chochlik usunął logikę tych klas. Zapomniał natomiast wyczyścić testy.

Twoim zadaniem jest uzupełneinie metod w obu klasach na podstawie tego, co znajdziesz w testach, tak aby nie zmieniając kodu testów przeszły one "na zielono".
Powodzenia!
